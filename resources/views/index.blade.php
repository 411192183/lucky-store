<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
  </head>
  <body>

    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Lucky Store</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="/">Home</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="/penjualan">Penjualan</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="/barang">Barang</a>
                </li>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="/pelanggan">Pelanggan</a>
                </li>
            </ul>
            </div>
        </div>
    </nav>

    <h1 class="mt-3">Data Barang</h1>

    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">No</th>
                <th scope="col">Kode Barang</th>
                <th scope="col">Nama Barang</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Stok Barang</th>
                <th scope="col">Harga Barang</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td>A001</td>
                <td>Blender</td>
                <td>Alat pelumat</td>
                <td>20</td>
                <td>450000</td>

                <td>
                    <a href="" class="badge badge-success">edit</a>
                    <a href="" class="badge badge-danger">delete</a>
                </td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>A002</td>
                <td>Rice Cooker</td>
                <td>Alat Penanak Nasi</td>
                <td>30</td>
                <td>250000</td>

                <td>
                    <a href="" class="badge badge-success">edit</a>
                    <a href="" class="badge badge-danger">delete</a>
                </td>
            </tr>
            <tr>
                <th scope="row">3</th>
                <td>A003</td>
                <td>Air Purifier</td>
                <td>Alat Pembersih Udara Ruangan</td>
                <td>50</td>
                <td>800000</td>

                <td>
                    <a href="" class="badge badge-success">edit</a>
                    <a href="" class="badge badge-danger">delete</a>
                </td>
            </tr>
            <tr>
                <th scope="row">4</th>
                <td>A004</td>
                <td>UV Cabinet</td>
                <td>Alat Penetralisir</td>
                <td>50</td>
                <td>800000</td>

                <td>
                    <a href="" class="badge badge-success">edit</a>
                    <a href="" class="badge badge-danger">delete</a>
                </td>
            </tr>
        </tbody>
    </table>
  </body>
</div>
</html>
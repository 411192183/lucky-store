<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
  //  return view('index');
// });

// Route::get('/penjualan', function () {
  //  return view('penjualan');
// });

// Route::get('/barang', function () {
  //  return view('barang');
// });

Route::get('/', 'PagesController@home');
Route::get('/penjualan', 'PagesController@penjualan');
Route::get('/barang' , 'BarangController@index');
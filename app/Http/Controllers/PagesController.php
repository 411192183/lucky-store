<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function home()
    {
        return view('index');
    }

    public function penjualan()
    {
        return view('penjualan');
    }

    public function barang()
    {
        return view('barang');
    }

    public function pelanggan()
    {
        return view('pelanggan');
    }
}
